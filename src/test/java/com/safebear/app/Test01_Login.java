package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {
    @Test
    public void testLogin(){
        //Step 1 Confirm we are on Welcome page
        assertTrue(welcomePage.checkCorrectPage());

        //Step 2 click on Login link and Login page loads
        welcomePage.clickLoginLink();

        //Step 3 confirm on Login page
        assertTrue(loginPage.checkCorrectPage());

        //Step 4 login with valid credentials
        loginPage.login("testuser","testing");

        //Step 5 check on User page
        assertTrue(userPage.checkCorrectPage());
    }
}
